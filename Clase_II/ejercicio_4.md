<h4>Ejercicio 4:</h4>

Crea un diccionario vacío para guardar los datos de un paciente y, posteriormente, guarda en él su edad (40), sexo (‘M’) y si es diabético (True). Una vez añadidos los datos, ¿cuáles serán las claves y los valores del diccionario? ¿Cuántos datos tenemos sobre este paciente?

Extra: ¿puedes crear un diccionario que almacene los datos de **dos** pacientes?
