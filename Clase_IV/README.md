* #### Clase IV. Python: Manejo de datos

* #### Requisitos
    *   Instalar numpy, pandas y matplotlib usando *pip*. Para eso, abrir una terminal de Linux o MacOS e instalar con el comando *pip install numpy pandas matplotlib*. La instalación en Windows es ligeramente distinta; pueden seguir [este tutorial](https://datatofish.com/install-package-python-using-pip/).
    *   Si lo anterior no funcionó, descargar [pandas](https://files.pythonhosted.org/packages/07/cf/1b6917426a9a16fd79d56385d0d907f344188558337d6b81196792f857e9/pandas-0.25.1.tar.gz) y [matplotlib](https://files.pythonhosted.org/packages/12/d1/7b12cd79c791348cb0c78ce6e7d16bd72992f13c9f1e8e43d2725a6d8adf/matplotlib-3.1.1.tar.gz). Luego, abrir una terminal de Linux o MacOS e instalar con el comando *pip install <path-al-archivo>*, comenzando por pandas y luego matplotlib.
    *   Descargar los archivos ['Happy numbers'](https://gitlab.com/NPalopoli/python-biocomp/raw/master/Clase_IV/happynumbers.txt?inline=false) y ['Prime numbers'](https://gitlab.com/NPalopoli/python-biocomp/raw/master/Clase_IV/primenumbers.txt?inline=false).
    *   Además, deben descargar los archivos [surveys.csv](https://ndownloader.figshare.com/files/10717177) y [species.csv](https://ndownloader.figshare.com/files/3299483).  
  
* #### Contenidos
    *   [Lectura y escritura de datos estructurados.](https://datacarpentry.org/python-ecology-lesson-es/02-starting-with-data/index.html)
    *   [Manipulación de dataframes](https://datacarpentry.org/python-ecology-lesson-es/03-index-slice-subset/index.html)
    *   [Tipos de datos y formatos / escribir archivo .csv](https://datacarpentry.org/python-ecology-lesson-es/04-data-types-and-format/index.html)
    *   [Combinando dataframes](https://datacarpentry.org/python-ecology-lesson-es/05-merging-data/index.html)
    *   [Tablas de datos con Pandas](https://datacarpentry.org/python-ecology-lesson-es/08-putting-it-all-together/index.html)
    *   Datos en grupos.
    *   Datos faltantes.
    *   Estadística descriptiva.
    *   [Tabla 'surveys.csv'](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/surveys.csv)
    *   [Tabla 'species.csv'](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/species.csv)
    *   [Archivo Happy numbers](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/happynumbers.txt)
    *   [Archivo Prime numbers](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/primenumbers.txt)
    * **Ejercicios** :arrow_down:
        * [Ejercicio de repaso (ejercicio_8)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/ejercicio_8.md)
        * [Primer reto de gráficos (ejercicio_graficos_0.md)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/ejercicio_graficos_0.md)
        * [Segundo desafío (ejercicio_graficos_1.md)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/ejercicio_graficos_1.md)
        * [Ejercicio de integración (ejercicio_graficos_2.md)](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/ejercicio_graficos_2.md)
    * Soluciones
        * [soluciones_ ejercicio_graficos_1.md ](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/soluciones_%20ejercicio_graficos_1.md)
    
