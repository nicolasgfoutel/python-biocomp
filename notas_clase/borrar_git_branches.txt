# Borrar branches en git (local y/o remoto)
# https://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-locally-and-remotely/2003515#2003515

To delete the local branch use one of the following:

git branch -d branch_name
git branch -D branch_name
Note: The -d option is an alias for --delete, which only deletes the branch if it has already been fully merged in its upstream branch. You could also use -D, which is an alias for --delete --force, which deletes the branch "irrespective of its merged status." [Source: man git-branch]

To delete the remote branch use:

git push <remote_name> --delete <branch_name>
(e.g.: git push origin --delete patch-1 )
